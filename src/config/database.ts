import * as mongoose from "mongoose";

const DBConnection = process.env.DB_STRING;
mongoose.connect(DBConnection, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
});

mongoose.connection.on('connected', () => {
    console.log('Database connected');
});