import { Router } from 'express';
import * as passport from 'passport';
import AuthController from '../controllers/AuthController';
export const routes = Router();
import validator from '../validator';
routes.get('/protected', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    res.status(200).json({ success: true, msg: "You are successfully authenticated to this route!" });
});

routes.get("/logout", function (req, res) {
    //logout
});

routes.post('/login', validator('authSchema'), AuthController.login);

routes.post('/register', validator('authSchema'), AuthController.register);
