import { Router } from 'express';
import { routes } from './auth'
import { NotFound } from 'http-errors';

const router = Router();

router.use('/auth', routes);

router.use((req, res, next) => {
    next(NotFound("This Route does not exist."));
})

router.use((err, req, res, next) => {
    res.send({
        error: {
            status: err.status || 500,
            message: err.message
        }
    })
})
export default router;