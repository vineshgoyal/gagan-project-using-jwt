import { randomBytes, pbkdf2Sync } from 'crypto';
import * as jsonwebtoken from 'jsonwebtoken';

export const verifyPassword = (password, hash, salt) => {
  var hashVerify = pbkdf2Sync(password, salt, 10000, 64, 'sha512').toString('hex');
  return hash === hashVerify;
}

export const genPassword = (password) => {
  var salt = randomBytes(32).toString('hex');
  var genHash = pbkdf2Sync(password, salt, 10000, 64, 'sha512').toString('hex');

  return {
    salt: salt,
    hash: genHash
  };
}

export const genToken = (user) => {
  const _id = user._id;

  const payload = {
    sub: _id,
    iat: Date.now()
  };

  const signedToken = jsonwebtoken.sign(payload, process.env.JWT_SECRET, { expiresIn: '1d' });

  return {
    token: "Bearer " + signedToken
  }
}
