import { Schema, model } from "mongoose";
const AccountSchema = new Schema({
  username: {
    type: String,
    required: true,
    unique: true
  },
  location: String,
  role: String,
  hash: String,
  salt: String
});

model("Account", AccountSchema);
