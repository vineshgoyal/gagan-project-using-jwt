import * as express from "express";
import * as path from "path";
import * as passport from "passport";
import { config } from 'dotenv';
config();

import "./config/database";
import './models';
import "./config/passport";
import routes from './routes';
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(passport.initialize());

app.use(express.static(path.join(__dirname, 'public')));

app.get("/", function (req, res) {
  res.render("index", { title: "Hey" });
});

app.use(routes);

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running in http://localhost:${PORT}`);
});
