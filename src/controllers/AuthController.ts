import { model } from 'mongoose';
import { genPassword, genToken, verifyPassword } from '../helpers/util';
import { Conflict, Unauthorized } from 'http-errors';
const Account = model('Account');

export default class AuthController {
    static login(req, res, next) {
        Account.findOne({ username: req.body.username })
            .then((user: any) => {
                if (!user) {
                    throw Unauthorized("User not registered.");
                }
                const isValid = verifyPassword(req.body.password, user.hash, user.salt);
                if (isValid) {
                    const tokenObject = genToken(user);
                    res.status(200).json({ success: true, token: tokenObject.token });
                } else {
                    throw Unauthorized("Username/Password not valid.");
                }
            })
            .catch((err) => {
                next(err);
            });
    }
    static register(req, res, next) {
        Account.findOne({
            username: req.body.username
        }).then((user: any) => {
            if (!user) {
                const saltHash = genPassword(req.body.password);

                const salt = saltHash.salt;
                const hash = saltHash.hash;

                const newUser = new Account({
                    username: req.body.username,
                    hash: hash,
                    salt: salt
                });
                try {
                    newUser.save()
                        .then((user) => {
                            res.json({ success: true, user: user });
                        });

                } catch (err) {
                    res.json({ success: false, msg: err });
                }
            } else {
                throw Conflict(`${req.body.username} is already been registered.`);
            }
        }).catch((err) => {
            next(err);
        });


    }
}