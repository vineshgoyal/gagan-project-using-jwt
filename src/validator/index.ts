import authSchema from './auth.schema';
const validators = {
    authSchema
}
export default (validatorName) => {
    return (req, res, next) => {
        if (validators[validatorName]) {
            const { error, value } = validators[validatorName].validate(req.body);
            if (error) {
                next(error);
            } else {
                next();
            }
        } else {
            next();
        }
    }
}